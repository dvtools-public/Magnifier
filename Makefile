COMPILER = cc
SOURCE = xlupe.c
CFLAGS = -O3
LDFLAGS = -lXm -lX11 -lXt -lXext -lXmu -lXpm
TARGET = ./dvmag
INSTDIR = /usr/local/bin

.PHONY: all clean install uninstall

all: $(TARGET)

$(TARGET): $(SOURCE) 
	$(COMPILER) $(CFLAGS) $^ $(LDFLAGS) -o $@
clean:
	rm -f $(TARGET)

install: $(TARGET)
	install -m 755 $(TARGET) $(INSTDIR)

uninstall:
	rm -f $(INSTDIR)/$(notdir $(TARGET))

