### Magnifier

Desktop magnifying glass for X11 with a Motif GUI, based on 'xlupe' by Thomas Runge.

This is an opinionated fork that strips out many unwanted features and cleans up the user interface. Padding, attachment points, labels, colors and shadows have been modified for a nicer look and feel. The old imake build system has been replaced by a much smaller Makefile. The manual page and Xaw/Athena widget support are also being removed.




![screenshot](screenshots/dvmag_sample.png)

(Follows mouse pointer until frozen)

#### Features

- Make small thing on screen bigger.
- Rotate, Mirror and Freeze the current frame.
- Click and drag to select frame and freeze.
- Slider to adjust scaling.

#### Requirements

- Motif 2.3.8
- X11/Xlib
- libXpm
- libXmu
- libXext

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvmag
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```

#### License

Please see the original license text and author credits preserved at the top of xlupe.c