   /*-
    * Copyright (c) 1999/2000/2001 Thomas Runge (runge@rostock.zgdv.de)
    * All rights reserved.
    *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions
    * are met:
    * 1. Redistributions of source code must retain the above copyright
    *    notice, this list of conditions and the following disclaimer.
    * 2. Redistributions in binary form must reproduce the above copyright
    *    notice, this list of conditions and the following disclaimer in the
    *    documentation and/or other materials provided with the distribution.
    *
    * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
    * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
    * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    * SUCH DAMAGE.
    *
    */


#define MAXZOOM 50

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include <math.h>
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <X11/Shell.h>
#include <X11/cursorfont.h>
#include <X11/Xmu/WinUtil.h>

/* Includes all motif widgets. */
/* This is probably bad for static linking. */
/* Fix later to only include required widgets. */
#include <Xm/XmAll.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>


#include <X11/xpm.h>
#include "lupe.xpm"

#include "lupe.xbm"

static char const id[] = "$Id: xlupe Version 1.2 by Thomas Runge (runge@rostock.zgdv.de) $";

/*
#define APPNAME "xlupe"
#define APPCLASS "XLupe"
#define APPTITLE "xlupe"
*/

#define APPNAME "Magnifier"
#define APPCLASS "Magnifier"
#define APPTITLE "Magnifier"

#define NEW_WIN True
#define OLD_WIN False

Widget toplevel, drawW, formW, sliderW;
Widget rotateB, switchB, zoomB, freezeB, jumpB, infoB, saveB, quitB;
XImage* image;
XtAppContext app_con;

Widget big_formW, pic_formW;
Widget big_frameW, pic_frameW;

char *viewer = NULL;

unsigned int position = 0, position1 = 0, position2 = 0, position3 = 0;
unsigned int pcounter = 0;
unsigned int psequence[] = { 0, 6, 3, 5 };   

unsigned int width, height, zoomx, zoomy;
unsigned int zoomflags = 3;
int winheight, winwidth;
int dspheight, dspwidth;
unsigned int LUPEWIDTH = 480;
unsigned int LUPEHEIGHT = 370;
int lupe_x = 0, lupe_y = 0, init = 0;

int N0, N1;
Display *dsp;
GC gc, ovlgc;
XtWorkProcId wpid;
XtIntervalId counterId;
XtIntervalId limitId;
Pixmap lupe_pm;
Window rwin, window;
Boolean ready, frozen, old_frozen, smooth, have_shm, changed, debug;
Pixel bg_pixel;
XShmSegmentInfo shminfo;
unsigned long limit;
int counter;
int ErrorFlag;

typedef struct
{
 char *winname;
 char *vistype;
 int depth;
 Visual *new_visual;
 Visual *this_visual;
 Colormap cmap;
 Window win;
 int x;
 int y;   
 int width;
 int height;
} visinfo;
visinfo vinf, definf;

typedef struct
{
 Dimension width, height;
 Position x, y;
 Boolean set;
} win_attr;
win_attr old_win;

int NewInterface();
void MakeAthenaInterface();
void MakeMotifInterface();
void PrepareForJump();
void GetWinAttribs();
int HandleXError(Display*, XErrorEvent*);
void CalcTables();
int check_for_xshm();
XImage *alloc_xshm_image(int, int, int);
void destroy_xshm_image(XImage*);
void zoomCB(Widget, XtPointer, XtPointer);
void rotateCB(Widget, XtPointer, XtPointer);
void switchCB(Widget, XtPointer, XtPointer);
void freezeCB(Widget, XtPointer, XtPointer);

void quitCB(Widget, XtPointer, XtPointer);
void sliderCB(Widget, XtPointer, XtPointer);

void sliderScrollCB(Widget, XtPointer, XtPointer);

Boolean drawCB(XtPointer);
void frozenDraw();
void counterCB(XtPointer, XtIntervalId*);
void limitCB(XtPointer, XtIntervalId*);
void setXYtitle();

void resizeCB(Widget, XtPointer, XtPointer);
void exposeCB(Widget, XtPointer, XtPointer);

void zoom_8(XImage *newimage, XImage *image, int width, int height);
void zoom_normal(XImage *newimage, XImage *image, int width, int height);
void zoom_smooth(XImage *newimage, XImage *image, int width, int height);
void usage(char *progname);
Window Select_Window();
int FillVisinfo(Window window);

static String fbres[] =
{
 "*.traversalOn:	      False",

 "*.beNiceToColormap:         0",
 "*justify:		      right",

 NULL
};

void pix2rgb(unsigned char *pix, unsigned char *rgb, int depth)
{
   
 if (depth>=24)
 {
  memcpy(rgb, pix, 3);
  return;  
 }
 if (depth==16)
 {
  rgb[0] = ((pix[N1]>>3)*255)/31;
  rgb[1] = ((((pix[N1]&7)<<3)|(pix[N0]>>5))*255)/63;
  rgb[2] = ((pix[N0]&31)*255)/31;
  return;  
 }   
 if (depth==15)
 {
  rgb[0] = ((pix[N1]>>2)*255)/31;
  rgb[1] = ((((pix[N1]&3)<<3)|(pix[N0]>>5))*255)/31;
  rgb[2] = ((pix[N0]&31)*255)/31;    
  return;  
 }   
}

void rgb2pix(unsigned char *rgb, unsigned char *pix, int depth)
{
 if (depth>=24)
 {
  memcpy(pix, rgb, 3);
  return;  
 }
 if (depth==16)
 {
  pix[N0] = (rgb[2]&248)>>3 | (rgb[1]&28)<<3;
  pix[N1] = (rgb[1]&224)>>5 | (rgb[0]&248);        
  return;  
 }
 if (depth==15)
 {
  pix[N0] = (rgb[2]&248)>>3 | (rgb[1]&56)<<2;
  pix[N1] = (rgb[1]&192)>>6 | (rgb[0]&248)>>1;    
  return;  
 }
}

void quitCB(Widget widget, XtPointer clientData, XtPointer callData)
{
 exit(EXIT_SUCCESS);
}



int HandleXError(Display *dsp, XErrorEvent *event)
{
 char msg[80];

 if(debug)
 {
  XGetErrorText(dsp, event->error_code, msg, 80);
  fprintf(stderr, "Error code %s\n", msg);
 }

 ErrorFlag = 1;
 return 0;
}

/*
 * Check if the X Shared Memory extension is available.
 * Return:  0 = not available
 *          1 = shared XImage support available
 *          2 = shared Pixmap support available also
 */
int check_for_xshm()
{
 int major, minor, ignore;
 Bool pixmaps;

 if(XQueryExtension(dsp, "MIT-SHM", &ignore, &ignore, &ignore))
 {
  if(XShmQueryVersion(dsp, &major, &minor, &pixmaps) == True)
   return(pixmaps==True) ? 2 : 1;
  else
   return 0;
 }
 else
  return 0;
}

/*
 * Allocate a shared memory XImage.
 */
XImage *alloc_xshm_image(int width, int height, int depth)
{
 XImage *img;

 img = XShmCreateImage(dsp, vinf.this_visual, depth, ZPixmap, NULL, &shminfo,
                       width, height);
 if(img == NULL)
 {
  if(debug)
   printf("XShmCreateImage failed!\n");
  return NULL;
 }

 shminfo.shmid = shmget(IPC_PRIVATE, img->bytes_per_line * img->height,
                        IPC_CREAT|0777 );
 if(shminfo.shmid < 0)
 {
  if(debug)
   perror("shmget");
  XDestroyImage(img);
  img = NULL;
  if(debug)
   printf("Shared memory error (shmget), disabling.\n");
  return NULL;
 }

 shminfo.shmaddr = img->data = (char*)shmat(shminfo.shmid, 0, 0);
 if(shminfo.shmaddr == (char *) -1)
 {
  if(debug)
   perror("alloc_xshm_image");
  XDestroyImage(img);
  img = NULL;
  if(debug)
   printf("shmat failed\n");
  return NULL;
 }

 shminfo.readOnly = False;
 ErrorFlag = 0;
 XShmAttach(dsp, &shminfo);
 XSync(dsp, False);

 if(ErrorFlag)
 {
  /* we are on a remote display, this error is normal, don't print it */
  XFlush(dsp);
  ErrorFlag = 0;
  XDestroyImage(img);
  shmdt(shminfo.shmaddr);
  shmctl(shminfo.shmid, IPC_RMID, 0);
  return NULL;
 }

 shmctl(shminfo.shmid, IPC_RMID, 0); /* nobody else needs it */

 return img;
}

void destroy_xshm_image(XImage *img)
{
 XShmDetach(dsp, &shminfo);
 XDestroyImage(img);
 shmdt(shminfo.shmaddr);
}

static XPoint old_p[5];
void btnDownCB(Widget w_, XtPointer clientData, XEvent *event)
{
 int x, y;
 unsigned int w, h;

 if(event->xbutton.button != Button1)
  return;

 x = event->xbutton.x_root;
 y = event->xbutton.y_root;
 if (position3)
 {
  h = (unsigned int) ((width+zoomx-1) / zoomx);
  w = (unsigned int) ((height+zoomy-1) / zoomy);
 }
 else
 {
  w = (unsigned int) ((width+zoomx-1) / zoomx);
  h = (unsigned int) ((height+zoomy-1) / zoomy);
 }
 w = (w+3)>>1;
 h = (h+3)>>1;
   
 old_p[0].x = x - w;
 old_p[0].y = y - h;
 old_p[1].x = x + w;
 old_p[1].y = old_p[0].y;
 old_p[2].x = old_p[1].x;
 old_p[2].y = y + h;
 old_p[3].x = old_p[0].x;
 old_p[3].y = old_p[2].y;
 old_p[4]   = old_p[0];

 XDrawLines(dsp, vinf.win, ovlgc, old_p, 5, CoordModeOrigin);
}

void hexaConvert(unsigned int num, char *val)
{
  unsigned int i;
  i = num/16;
  val[0] = (i<10)? '0'+i : 'A'+(i-10);
  i = num%16;
  val[1] = (i<10)? '0'+i : 'A'+(i-10);   
}

void DrawString(Display *dpy, Widget wid, GC gc, int x, int y, char * str, int len)
{
   XDrawString(dpy, XtWindow(wid), gc, x, y, str, len);
}

void btnUpCB(Widget w_, XtPointer clientData, XEvent *event)
{
 char buffer[80];
 char value[8];   
 XColor xc;
 XImage *xim;
/*
 if(event->xbutton.button == Button2) {
  if (!frozen) return;
  frozenDraw();
 }
*/ 
 if(event->xbutton.button == Button3) {
  if (!frozen) return;
  frozenDraw();
  XFlush(dsp);
  xim = XGetImage (dsp, XtWindow(drawW), event->xbutton.x, event->xbutton.y,
		   1, 1, AllPlanes, ZPixmap);
  xc.pixel = XGetPixel(xim, 0, 0);
  XQueryColor(dsp, vinf.cmap, &xc);
  hexaConvert(xc.red>>8, value);
  hexaConvert(xc.green>>8, value+2);
  hexaConvert(xc.blue>>8, value+4);
  value[6] = '\0';
  sprintf(buffer, " Pixel (%d,%d) color %s",
    (event->xbutton.x<0)? 0:event->xbutton.x/zoomx, 
    (event->xbutton.y<0)? 0:event->xbutton.y/zoomy, value);
  XDestroyImage(xim);
  XSetBackground(dsp, gc, WhitePixelOfScreen(XtScreen(toplevel)));
  if (event->xbutton.y>LUPEHEIGHT/2)
    DrawString(dsp, drawW, gc, 0, 11, buffer, strlen(buffer));
  else
    DrawString(dsp, drawW, gc, 0, LUPEHEIGHT-2, buffer,
		     strlen(buffer));      
  return;
 }

 if(event->xbutton.button != Button1)
  return;

 XDrawLines(dsp, vinf.win, ovlgc, old_p, 5, CoordModeOrigin);
 drawCB(clientData);
   
 frozen = False;
 freezeCB(freezeB, clientData, NULL);      
}

void btnMotionCB(Widget w_, XtPointer clientData, XEvent *event)
{
 int x, y;
 unsigned int w, h;
 XPoint p[5];

 x = event->xbutton.x_root;
 y = event->xbutton.y_root;
 if (position3)
 {
  h = (unsigned int) ((width+zoomx-1) / zoomx);
  w = (unsigned int) ((height+zoomy-1) / zoomy);
 }
 else
 {
  w = (unsigned int) ((width+zoomx-1) / zoomx);
  h = (unsigned int) ((height+zoomy-1) / zoomy);
 } 
 w = (w+3)>>1;
 h = (h+3)>>1;
   
 p[0].x = x - w;
 p[0].y = y - h;
 p[1].x = x + w;
 p[1].y = p[0].y;
 p[2].x = p[1].x;
 p[2].y = y + h;
 p[3].x = p[0].x;
 p[3].y = p[2].y;
 p[4]   = p[0];

 XDrawLines(dsp, vinf.win, ovlgc, old_p, 5, CoordModeOrigin);
 XDrawLines(dsp, vinf.win, ovlgc, p, 5, CoordModeOrigin);

 memcpy(&old_p, &p, 5*sizeof(XPoint));
}

void keyReleaseCB(Widget w_, XtPointer clientData, XEvent *event)
{
 KeySym keysym;
 char buffer[2];
 int shift;
   
 if (event->type != KeyRelease) return;
 XLookupString((XKeyEvent *) event, buffer, 1, &keysym, NULL);
 shift = (((XKeyEvent *) event)->state & ControlMask)? 10 : 1;
   
 switch(keysym) {
  case XK_q:
  case XK_Q:
  case XK_Escape:    
    exit(0);

  case XK_m:
  case XK_M:    
    position1 = 1-position1;
    position = (position&6) | position1;

    XtVaSetValues(switchB, XmNset, position1, NULL);    

    frozenDraw();
    break;
  case XK_r:
  case XK_R:
    pcounter = (pcounter+1)%4;
    position = psequence[pcounter];
    position1 = (position&1);
    position2 = (position&2)>>1;
    position3 = (position&4)>>2;
    frozenDraw();    
    break;    

  case XK_space: 
  case XK_f:     
  case XK_F:         
    freezeCB(freezeB, clientData, NULL); 
    break;

 }
}

void zoomCB(Widget w, XtPointer clientData, XtPointer callData)
{
 Boolean val;
 zoomflags = 1 + (zoomflags%3);

 if (zoomflags==1)
 {
  XmScaleSetValue(sliderW, zoomx);
 }
 if (zoomflags==2)
 {
  XmScaleSetValue(sliderW, zoomy);
 }   
 if (zoomflags==3)
 {
  zoomx = (zoomx + zoomy)/2;
  zoomy = zoomx;    
  XmScaleSetValue(sliderW, zoomx);
 }
 XtVaSetValues(zoomB, XmNwidth, 19, NULL);

 setXYtitle();       
}
   
void sliderCB(Widget w, XtPointer clientData, XtPointer callData)
{
 XmScaleCallbackStruct *sccb;

 sccb = (XmScaleCallbackStruct*) callData;

 if(zoomx != sccb->value && (zoomflags&1))
  zoomx = sccb->value;

 if(zoomy != sccb->value && (zoomflags&2))
  zoomy = sccb->value;   

 XtSetKeyboardFocus(toplevel, drawW);


 setXYtitle();       
 frozenDraw();   
 return;
}


void counterCB(XtPointer clientData, XtIntervalId* id)
{
 printf("frame count: %.2f fps\n", counter/10.);
 counter = 0;
 counterId = XtAppAddTimeOut(app_con, 10000, counterCB, (XtPointer)NULL);
}

void limitCB(XtPointer clientData, XtIntervalId* id)
{
 ready = True;
 limitId = XtAppAddTimeOut(app_con, limit, limitCB, (XtPointer)NULL);
}

void rotateCB(Widget w, XtPointer clientData, XtPointer callData)
{
 pcounter = (pcounter+1)%4;
 position = psequence[pcounter];
 position1 = (position&1);
 position2 = (position&2)>>1;
 position3 = (position&4)>>2;
 frozenDraw();
}

void switchCB(Widget w, XtPointer clientData, XtPointer callData)
{
 position1 = 1-position1;
 position = (position&6) | position1;
 frozenDraw();
}

Boolean drawCB(XtPointer clientData)
{
 XImage *newimage;
 Window dummy_window1, dummy_window2;
 int x, y;
 static int p_x, p_y, r_x, r_y;
 unsigned int dummy_int;
 unsigned int w, h;
 static size_t data_size=0;

 if(limit)
 {
  if(!ready)
   return(False);
  else
   ready = False;
 }

 if (init && frozen) {
  if (position3)
  {	 
   h = (width+zoomx-1)/zoomx;
   w = (height+zoomy-1)/zoomy;
   x = lupe_x - (h>>1);
   y = lupe_y - (w>>1);   
  } else
  {
   w = (width+zoomx-1)/zoomx;
   h = (height+zoomy-1)/zoomy;     
   x = lupe_x - (w>>1);
   y = lupe_y - (h>>1);
  }
  if (x<0) x = 0;  
  if (y<0) y = 0;      
  goto capture;
 }
	
 XQueryPointer(dsp, vinf.win, &dummy_window1, &dummy_window2,
               &r_x, &r_y, &p_x, &p_y, &dummy_int);

 if (position3)
 {	
  h = (unsigned int) ((width+zoomx-1) / zoomx);
  w = (unsigned int) ((height+zoomy-1) / zoomy);
 }
 else
 {	
  w = (unsigned int) ((width+zoomx-1) / zoomx);
  h = (unsigned int) ((height+zoomy-1) / zoomy);
 }   
 x = p_x - (w>>1);
 y = p_y - (h>>1);

 if(x<0)
  x = 0;
 if(y<0)
  y = 0;

 if(x+w>winwidth)
  x = winwidth-w;
 if(y+h>winheight)
  y = winheight-h;

 if(vinf.win != rwin)
 {
  if(r_x+w>dspwidth)
   x = dspwidth-w-r_x+p_x;
  if(r_y+h>dspheight)
   y = dspheight-h-r_y+p_y;
 }

 lupe_x = x + (w>>1);
 lupe_y = y + (h>>1);
   
capture:

 init = 0;   
   
 if(w>winwidth)
  w = winwidth;
 if(h>winheight)
  h = winheight;

 newimage = XGetImage(dsp, vinf.win, x, y, w, h, AllPlanes, ZPixmap);

 if(ErrorFlag)
 {
  ErrorFlag = 0;
  return(False);
 }

 if(newimage == NULL)
 {
  if(debug)
   fprintf(stderr, "Couldn't get the new image...\n");
  return(False);
 }

 if(changed)
 {
  if(have_shm)
  {
   if(image)
    destroy_xshm_image(image);
   image = alloc_xshm_image(width, height, newimage->depth);
   if(image == NULL)
   {
    if(debug)
     printf("remote display, disabling shared memory.\n");
    have_shm = False;
    goto goon;
   }
   data_size = image->bytes_per_line * image->height;
  }
  else
  {
   if(image)
    XDestroyImage(image);
   image = XCreateImage(dsp, vinf.this_visual, newimage->depth,
                        ZPixmap, 0, 0, width, height,
                        newimage->bitmap_pad, 0);
   data_size = image->bytes_per_line * image->height;
   image->data = XtMalloc(data_size);
  }
  changed = False;
 }

 if(vinf.depth == 8)
  memset(image->data, bg_pixel, data_size);
 else
  memset(image->data, WhitePixelOfScreen(XtScreen(toplevel)), data_size);

 if(debug)
  counter ++;

 if(zoomx==1 && zoomy==1 && !position)
 {
  memcpy(image->data, newimage->data, data_size);
 }
 else
 {
  /* zoom_8 is appr. 1/3 faster then zoom_normal */
  if(vinf.depth==8 && !position)
   zoom_8(newimage, image, w, h);
  else 
  {
    if (position3)      
     zoom_normal(newimage, image, h, w);
    else
     zoom_normal(newimage, image, w, h);
  }
 }

 if(have_shm)
  XShmPutImage(dsp, window, gc, image, 0, 0, 0, 0, width, height, False);
 else
  XPutImage(dsp, window, gc, image, 0, 0, 0, 0, width, height);

goon:

 XDestroyImage(newimage);

 XtSetKeyboardFocus(toplevel, drawW);

   
 if(frozen)
  return(True);

 return(False);
}

void zoom_8(XImage *newimage, XImage *image, int w, int h)
{
 char c;
 char *data;
 int x, y, dx, dy, xz, yz, fx, fy;

 data = image->data;
 xz = -zoomx;
 for(x = 0; x < w; x++)
 {
  xz += zoomx;
  yz = -zoomy;
  if (xz+zoomx-1<image->width)
   fx = zoomx;
  else
   fx = image->width - xz;
  for(y = 0; y < h; y++)
  {
   yz += zoomy;
   if (yz+zoomy-1<image->height)
    fy = zoomy;
   else
    fy = image->height - yz;
   c = newimage->data[x+y*newimage->bytes_per_line];
   for(dy=0; dy<fy; dy++)
    memset(&data[image->bytes_per_line*(yz+dy)+xz], (int)c, fx);
  }
 }
}

void frozenDraw()
{  
 if (frozen)
 {
  init = 1;
  drawCB(NULL);
 }
 XFlush(dsp);
}

void zoom_normal(XImage *newimage, XImage *image, int w, int h)
{
 char *c;
 char *data;
 int u, v, x1, y1, x, y, z, xz, yz, dx, dy, bpp, fx, fy;

 bpp = newimage->bits_per_pixel>>3;
 data = image->data;

 xz = -zoomx;
 for(x = 0; x < w; x++)
 {
  xz += zoomx;
  yz = -zoomy;
  if (xz+zoomx-1<image->width)
   fx = zoomx;
  else
   fx = image->width - xz;
  if (position1) x1 = w-1-x; else x1 = x;
  for(y = 0; y < h; y++)
  {
   yz += zoomy;
   if (yz+zoomy-1<image->height)
    fy = zoomy;
   else
    fy = image->height - yz;
   if (position2) y1 = h-1-y; else y1 = y;
   if (position3)      
     c = &(newimage->data[bpp*y1 + x1*newimage->bytes_per_line]);
   else
     c = &(newimage->data[bpp*x1 + y1*newimage->bytes_per_line]);
   u = bpp*xz + yz*image->bytes_per_line;       
   for(dy=0; dy<fy; dy++)
   {
    v = u + dy*image->bytes_per_line;
    if(dy==0)
     for(dx=0; dx<fx; dx++)
      memcpy(&data[v+dx*bpp], c, bpp);
    else
      memcpy(&data[v], &data[u], bpp*fx);	
   }
  }
 }
}


void freezeCB(Widget w, XtPointer clientData, XtPointer callData)
{
 frozen=!frozen;
 if (frozen)
 {
  XtVaSetValues(w, XmNset, True, NULL);
 }
 else
 {
  XtVaSetValues(w, XmNset, False, NULL);    
  wpid = XtAppAddWorkProc(app_con, drawCB, (XtPointer)NULL);
 }
 XFlush(dsp);
 return;
}


int FillVisinfo(Window win)
{
 XWindowAttributes winattr;
 Window root, junk;
 char *name;

 if(!win)
  return(OLD_WIN);

 if(!XGetWindowAttributes(dsp, win, &winattr))
 {
  fprintf(stderr, "Can't get window attributes.\n");
  return(OLD_WIN);
 }

 if(vinf.winname)
  XtFree(vinf.winname);

 if(XFetchName(dsp, win, &name))
  vinf.winname = XtNewString(name);
 else
 {
  if(win == rwin)
   vinf.winname = XtNewString(" (root window)");
  else
   vinf.winname = XtNewString(" (has no name)");
 }
 XFree(name);

 root = DefaultRootWindow(dsp);
 XTranslateCoordinates(dsp, win, root, 0, 0, &vinf.x, &vinf.y, &junk);
 vinf.width      = winattr.width;
 vinf.height     = winattr.height;
   
 if(winattr.depth    == vinf.depth &&
    winattr.visual   == vinf.this_visual &&
    winattr.colormap == vinf.cmap)
 {
  return(OLD_WIN);
 }

 vinf.depth      = winattr.depth;
 vinf.new_visual = winattr.visual;
 vinf.cmap       = winattr.colormap;

 if(winattr.depth    == definf.depth &&
    winattr.visual   == definf.this_visual &&
    winattr.colormap == definf.cmap)
 {
  vinf.win = rwin;
 }
 else
 {
  vinf.win = win;
  winheight = winattr.height;
  winwidth  = winattr.width;
 }
   
 if(vinf.vistype)
  XtFree(vinf.vistype);

 switch(winattr.visual->class)
 {
  case StaticGray:  vinf.vistype = XtNewString("StaticGray");
                     break;
  case GrayScale:   vinf.vistype = XtNewString("GrayScale");
                     break;
  case StaticColor: vinf.vistype = XtNewString("StaticColor");
                     break;
  case PseudoColor: vinf.vistype = XtNewString("PseudoColor");
                     break;
  case TrueColor:   vinf.vistype = XtNewString("TrueColor");
                     break;
  case DirectColor: vinf.vistype = XtNewString("DirectColor");
                     break;
  default:          vinf.vistype = XtNewString("*unknown* visualtype ?!?");
                     break;
 }

 return(NEW_WIN);
}


void exposeCB(Widget widget, XtPointer clientData, XtPointer callData)
{
 if(debug)
  printf("expose.\n");

 if(window && image && !changed)
   XPutImage(XtDisplay(widget), window, gc, image, 0, 0, 0, 0, width, height);

 return;
}


void resizeCB(Widget widget, XtPointer clientData, XtPointer callData)
{
 Dimension w, h, qw;

 if(debug)
  printf("resize.\n");

 XtVaGetValues(widget, XtNwidth,  &w,
                       XtNheight, &h,
                       NULL);
 width  = (unsigned int) w;
 height = (unsigned int) h;
 changed = True;

 FillVisinfo(window);
 
 frozenDraw();
 return;
}

void iconifyCB(Widget w, XtPointer clientData, XEvent *event)
{
 if(event->type == MapNotify)
 {
  frozen = old_frozen;
  if(!frozen)
   wpid = XtAppAddWorkProc(app_con, drawCB, (XtPointer)NULL);
  if(debug)
   printf("normal\n");
 }
 if(event->type == UnmapNotify)
 {
  old_frozen = frozen;
  frozen = True;
  if(debug)
   printf("iconified\n");
 }
 return;
}

void usage(char *progname)
{
 printf("\n  -h   Show this help text.\n");
 printf("  -b   Enable debug mode.\n");
 printf("  -e   Start in frozen mode.\n");
 printf("  -f   Limit to N frames per second.\n");   
 printf("  -g   Set geometry of window.\n");
 printf("  -z   Set initial zoom value.\n\n");

 exit(EXIT_FAILURE);
}

int NewInterface()
{
 Dimension minW, minH;
 changed = True;

 if(vinf.depth)
 {
  if(old_win.set)
   toplevel = XtVaAppCreateShell(APPNAME, APPCLASS,
        sessionShellWidgetClass, dsp,
                XtNdepth,    vinf.depth,
                XtNcolormap, vinf.cmap,
                XtNvisual,   vinf.new_visual,
                XtNwidth,    old_win.width,
                XtNheight,   old_win.height,
                XtNx,        old_win.x,
                XtNy,        old_win.y,
                NULL);
  else
   toplevel = XtVaAppCreateShell(APPNAME, APPCLASS,
        sessionShellWidgetClass, dsp,
               XtNdepth,    vinf.depth,
               XtNcolormap, vinf.cmap,
               XtNvisual,   vinf.new_visual,
               NULL);
 }
 else
 {
  toplevel = XtVaAppCreateShell(APPNAME, APPCLASS,
        sessionShellWidgetClass, dsp, NULL);
 }

 XtVaSetValues(toplevel,
               XtNallowShellResize, False,
               XtNtitle,            APPTITLE,
               XtNiconPixmap,       lupe_pm,
               NULL);



 MakeMotifInterface();


 XtVaGetValues(drawW, XtNwidth,  &minW,
                      XtNheight, &minH,
                      XtNdepth,  &(vinf.depth),
                      NULL);
 if(debug)
  printf("using depth %d\n", vinf.depth);

 width  = (int)minW;
 height = (int)minH;

 XtAddEventHandler(toplevel, StructureNotifyMask, False,
                   (XtEventHandler)iconifyCB, (XtPointer)NULL);

 if (!frozen)
  wpid = XtAppAddWorkProc(app_con, drawCB, (XtPointer)NULL);

 if(debug)
  counterId = XtAppAddTimeOut(app_con, 10000, counterCB, (XtPointer)NULL);

 if(limit)
  limitId = XtAppAddTimeOut(app_con, limit, limitCB, (XtPointer)NULL);

 vinf.this_visual = vinf.new_visual;

 XtRealizeWidget(toplevel);

 window = XtWindow(drawW);
 XtVaGetValues(toplevel, XtNbackground, &bg_pixel, NULL);

 XFreeGC(dsp, gc);
 gc = XCreateGC(dsp, window, 0, NULL);

 XtAddEventHandler(drawW, ButtonPressMask, False,
                   (XtEventHandler)btnDownCB, (XtPointer)NULL);
 XtAddEventHandler(drawW, ButtonReleaseMask, False,
                   (XtEventHandler)btnUpCB, (XtPointer)NULL);
 XtAddEventHandler(drawW, Button1MotionMask, False,
                   (XtEventHandler)btnMotionCB, (XtPointer)NULL);
 XtAddEventHandler(drawW, KeyPressMask|KeyReleaseMask, True,
                   (XtEventHandler)keyReleaseCB, (XtPointer)NULL);

 XtSetKeyboardFocus(toplevel, drawW);
		
 return(True);
}


void focusCB(Widget w, XtPointer clientData, XtPointer callData)
{
 XtSetKeyboardFocus(toplevel, drawW);
}


void MakeMotifInterface()
{
 Atom wmDeleteAtom;
 Widget last;
   
 formW =
   XtVaCreateManagedWidget("main_form",
                           xmFormWidgetClass,
                           toplevel,
			   XmNshadowThickness, 0,
                           NULL);

 big_frameW =      
      XtVaCreateManagedWidget("big_frame",
                              xmFrameWidgetClass,
                              formW,
                              XmNshadowType,       XmSHADOW_OUT,
                              XmNrightAttachment,  XmATTACH_FORM,
                              XmNrightOffset,      0,  
                              XmNleftAttachment,   XmATTACH_FORM,
                              XmNleftOffset,       0,
                              XmNtopAttachment,    XmATTACH_FORM,
                              XmNtopOffset,        0,
                              XmNbottomAttachment, XmATTACH_FORM,
                              XmNbottomOffset,     0,
			      XmNshadowThickness, 1,
                              NULL);

 big_formW =
   XtVaCreateManagedWidget("big_form",
                           xmFormWidgetClass,
                           big_frameW,
			   XmNshadowThickness, 0,
                           NULL);
 XtAddCallback(big_formW, XmNfocusCallback, focusCB, (XtPointer)NULL);   

 rotateB =
   XtVaCreateManagedWidget("rotate",
                           xmPushButtonWidgetClass,
                           big_formW,
                           XmNmarginWidth,      5,
                           XmNleftAttachment,   XmATTACH_FORM,
                           XmNleftOffset,       5,
                           XmNbottomAttachment, XmATTACH_FORM,
                           XmNbottomOffset,     5,
			   
			   XmNmarginWidth, 8,
                           XmNmarginHeight, 4,
			   
			   XtVaTypedArg, XmNlabelString, XmRString, "Rotate", 4,
			   XtVaTypedArg, XmNmnemonic, XmRString, "R", 4,
                           NULL);
 XtAddCallback(rotateB, XmNactivateCallback, rotateCB, (XtPointer)NULL);
 
 /* grab height of button */
 Dimension rotateB_h;
 XtVaGetValues(rotateB,
 	XmNheight, &rotateB_h,
 NULL);
 
 Pixel rotateB_bg, rotateB_ts, rotateB_bs;
 XtVaGetValues(rotateB,
 	XmNbackground, &rotateB_bg,
	XmNtopShadowColor, &rotateB_ts,
	XmNbottomShadowColor, &rotateB_bs,
 NULL);
 
 switchB =
   XtVaCreateManagedWidget("switch",
                           xmToggleButtonWidgetClass,
                           big_formW,
                           XmNmarginWidth,      5,
                           XmNleftAttachment,   XmATTACH_WIDGET,
                           XmNleftWidget,       rotateB,
			   XmNleftOffset, 3,
                           XmNbottomAttachment, XmATTACH_FORM,
                           XmNbottomOffset,     5,
			   XmNindicatorOn, 0,
			   
			   XmNmarginLeft, 6,
                           XmNmarginRight, 6,
			   
			   XmNheight, rotateB_h,
			   XmNbackground, rotateB_bg,
                           XmNtopShadowColor, rotateB_ts,
                           XmNbottomShadowColor, rotateB_bs,
			   
			   XtVaTypedArg, XmNlabelString, XmRString, "Mirror", 4,
			   XtVaTypedArg, XmNmnemonic, XmRString, "M", 4,
                           NULL);
 XtAddCallback(switchB, XmNdisarmCallback, switchCB, (XtPointer)NULL);
   
 freezeB =
   XtVaCreateManagedWidget("freeze",
                           xmToggleButtonWidgetClass,
                           big_formW,
                           XmNmarginWidth,      5,
                           XmNleftAttachment,   XmATTACH_WIDGET,
                           XmNleftWidget,       switchB,
			   XmNleftOffset, 3,
                           XmNbottomAttachment, XmATTACH_FORM,
                           XmNbottomOffset,     5,
			   XmNindicatorOn, 0,
			   
			   XmNmarginLeft, 6,
                           XmNmarginRight, 6,
			   
			   XmNheight, rotateB_h,
			   XmNbackground, rotateB_bg,
                           XmNtopShadowColor, rotateB_ts,
                           XmNbottomShadowColor, rotateB_bs,
			   
			   XtVaTypedArg, XmNlabelString, XmRString, "Freeze", 4,
			   XtVaTypedArg, XmNmnemonic, XmRString, "F", 4,
                           NULL);
 XtAddCallback(freezeB, XmNdisarmCallback, freezeCB, (XtPointer)NULL);


 Widget sep = XtVaCreateManagedWidget("sep", 
                           xmSeparatorWidgetClass,
			   big_formW,
                           
			   
			   XmNleftAttachment, XmATTACH_FORM,
			   XmNrightAttachment, XmATTACH_FORM,
			   
                           NULL);


 quitB =
   XtVaCreateManagedWidget("exit",
                           xmPushButtonWidgetClass,
                           big_formW,
                           XmNrightAttachment,  XmATTACH_FORM,
                           XmNrightOffset,      5,
                           XmNbottomAttachment, XmATTACH_FORM,
                           XmNbottomOffset,     5,
			   
			   XmNmarginWidth, 8,
			   XmNmarginHeight, 4,
			   
			   XtVaTypedArg, XmNlabelString, XmRString, "Quit", 4,
			   XtVaTypedArg, XmNmnemonic, XmRString, "Q", 4,
                           NULL);
 XtAddCallback(quitB, XmNactivateCallback, quitCB, (XtPointer)NULL);
   
 sliderW =
   XtVaCreateManagedWidget("slider",
                           xmScaleWidgetClass,
                           big_formW,
                           XmNdecimalPoints,    0,
                           XmNmaximum,          MAXZOOM,
                           XmNminimum,          1,
                           XmNvalue,            zoomx,
                           XmNshowValue,        False,
                           XmNscaleMultiple,    1,
                           XmNrightAttachment,  XmATTACH_FORM,
                           XmNrightOffset,      5,
                           XmNtopAttachment,    XmATTACH_FORM,
                           XmNtopOffset,        5,
                           XmNbottomAttachment, XmATTACH_WIDGET,
                           XmNbottomWidget,     quitB,
                           XmNbottomOffset,     10,
                           NULL);
 XtAddCallback(sliderW, XmNdragCallback, sliderCB, (XtPointer)NULL);
 XtAddCallback(sliderW, XmNvalueChangedCallback, sliderCB, (XtPointer)NULL);   

 pic_frameW =
   XtVaCreateManagedWidget("pic_frame",
                           xmFrameWidgetClass,
                           big_formW,
                           XmNshadowType,       XmSHADOW_IN,
                           XmNrightAttachment,  XmATTACH_WIDGET,
                           XmNrightWidget,      sliderW,
                           XmNrightOffset,      2,
                           XmNleftAttachment,   XmATTACH_FORM,
                           XmNleftOffset,       6,
                           XmNtopAttachment,    XmATTACH_FORM,
                           XmNtopOffset,        6,
                           XmNbottomAttachment, XmATTACH_WIDGET,
                           XmNbottomWidget,     quitB,
                           XmNbottomOffset,     11,
			   XmNshadowThickness, 2,
                           NULL);

 pic_formW =
   XtVaCreateManagedWidget("pic_form",
                           xmFormWidgetClass,
                           pic_frameW,
			   XmNshadowThickness, 0,
			   
			   XtVaTypedArg, XmNbackground, XmRString, "black", 4,
                           NULL);

 drawW =
   XtVaCreateManagedWidget("draw",
                           xmDrawingAreaWidgetClass,
                           pic_formW,
                           XmNrightAttachment,  XmATTACH_FORM,
                           XmNrightOffset,      2,
                           XmNleftAttachment,   XmATTACH_FORM,
                           XmNleftOffset,       2,
                           XmNtopAttachment,    XmATTACH_FORM,
                           XmNtopOffset,        2,
                           XmNbottomAttachment, XmATTACH_FORM,
                           XmNbottomOffset,     2,
			   
                           NULL);

 XtVaSetValues(drawW, XmNwidth,  LUPEWIDTH,
                      XmNheight, LUPEHEIGHT,
                      NULL);

 XtAddCallback(drawW, XmNexposeCallback, exposeCB, (XtPointer)NULL);
 XtAddCallback(drawW, XmNresizeCallback, resizeCB, (XtPointer)NULL);

 
 XtVaSetValues(sep,
	XmNtopAttachment, XmATTACH_WIDGET,
	XmNtopWidget, pic_frameW,
	XmNtopOffset, 5,
 NULL);


 wmDeleteAtom = XmInternAtom(dsp, "WM_DELETE_WINDOW", False);
 XmAddWMProtocolCallback(toplevel, wmDeleteAtom, quitCB, (XtPointer)NULL);

}


void GetWinAttribs()
{
 XtVaGetValues(toplevel, XtNx, &old_win.x,
                         XtNy, &old_win.y,
                         XtNwidth,  &old_win.width,
                         XtNheight, &old_win.height,
                         NULL);
 old_win.set = True;
}


void setXYtitle()
{
 char newtitle[40];
 sprintf(newtitle, "%s  %d x %d", APPNAME, zoomx, zoomy);
 XStoreName(dsp, XtWindow(toplevel), newtitle);
}


int main(int argc, char** argv)
{
 Boolean wants_shm;
 unsigned long XORvalue;
 int i, junk;

 image = NULL;
 zoomx = 5;
 zoomy = 5;
 changed = True;
 frozen = False;
 smooth = False;
 counter = 0;
 wants_shm = True;
 limit = 0;
 vinf.winname = NULL;
 vinf.depth = 0;
 vinf.vistype = NULL;
 old_win.set = False;



 XtToolkitInitialize();
 app_con = XtCreateApplicationContext();
 XtAppSetFallbackResources(app_con, fbres);
 dsp = XtOpenDisplay(app_con, NULL, APPNAME, APPCLASS, NULL, 0, &argc, argv);

 if(!dsp)
 {
  fprintf(stderr, "can't open display, exiting...\n");
  usage(argv[0]);
 }
 if (ImageByteOrder(dsp) == MSBFirst)
 { 
  N0 = 1;
  N1 = 0;    
 }
 else
 { 
  N0 = 0;
  N1 = 1;    
 }     

 rwin = DefaultRootWindow(dsp);
 vinf.new_visual = DefaultVisual(dsp, DefaultScreen(dsp));
 
 definf.depth       = DefaultDepth(dsp, DefaultScreen(dsp));
 definf.this_visual = DefaultVisual(dsp, DefaultScreen(dsp));
 definf.cmap        = DefaultColormap(dsp, DefaultScreen(dsp));
      
 dspheight = winheight = DisplayHeight(dsp, DefaultScreen(dsp));
 dspwidth  = winwidth  = DisplayWidth(dsp, DefaultScreen(dsp));
 XORvalue = (((unsigned long)1) << definf.depth) - 1;

 while((i = getopt(argc, argv, "bhef:x:y:z:l:v:")) != EOF)
  switch(i)
  {

   case 'b':
              debug = True;
              printf("Debugging mode.\n");
             break;
   
   case 'e':
              frozen = True;
             break;          

   case 'f':
              limit = 1000/strtol(optarg, (char **)NULL, 10);
             break;
   
   case 'z':
              zoomx = atoi(optarg);
              if (zoomx<=0) zoomx = 1;
              if (zoomx>MAXZOOM) zoomx = MAXZOOM;
              zoomy = zoomx;
             break;     

   case 'l':
              XParseGeometry(optarg, &lupe_x, &lupe_y, 
			     &LUPEWIDTH, &LUPEHEIGHT);
              lupe_x += LUPEWIDTH/2;
              lupe_y += LUPEHEIGHT/2;     
              LUPEWIDTH *= zoomx;
              LUPEHEIGHT *= zoomy;
              if (LUPEWIDTH<160) LUPEWIDTH = 160;
              if (LUPEWIDTH>dspwidth) LUPEWIDTH = dspwidth;
              if (LUPEHEIGHT<60) LUPEHEIGHT = 60;
              if (LUPEHEIGHT>dspheight) LUPEHEIGHT = dspheight;
              width = LUPEWIDTH;
              height = LUPEHEIGHT;
              init = 1;
             break;     
   
   case 'h':
   case '?':
   default:
             usage(argv[0]);
  }

 XSetErrorHandler(HandleXError);

 /* test if XShm available */
 if(!check_for_xshm())
 {
  if(debug)
   printf("shared memory extension not available\n");
  have_shm = False;
 }
 else
 {
  if(debug)
   printf("found shared memory extension\n");
  if(!wants_shm)
  {
   if(debug)
    printf("   but not using it...\n");
   have_shm = False;
  }
  else
   have_shm = True;
 }

 gc = XCreateGC(dsp, rwin, 0, NULL);
 ovlgc = XCreateGC(dsp, rwin, 0, NULL);
 XSetForeground(dsp, ovlgc, XORvalue);
 XSetFunction(dsp, ovlgc, GXxor);
 XSetSubwindowMode(dsp, ovlgc, IncludeInferiors);


 if(XpmCreatePixmapFromData(dsp, rwin, lupe_xpm,
                            &lupe_pm, NULL, NULL) < XpmSuccess)

 lupe_pm = XCreatePixmapFromBitmapData(dsp, rwin,
                          lupe_bits, lupe_width, lupe_height,
                          BlackPixel(dsp, DefaultScreen(dsp)),
                          WhitePixel(dsp, DefaultScreen(dsp)),
                          DefaultDepth(dsp, DefaultScreen(dsp)));

 old_frozen = frozen;
 NewInterface();
 GetWinAttribs();
 FillVisinfo(window);

 if (frozen)
 {
  XtVaSetValues(freezeB, XmNset, True, NULL);
 }
     
 
 if (frozen)
 {
  ready = True;
  XFlush(dsp);
  usleep(80000);    
  ready = True;
  drawCB(NULL);
 }
 setXYtitle();
   
 XtAppMainLoop(app_con);

 return(True);
}
